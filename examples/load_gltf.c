/*
 * xrdesktop
 * Copyright 2019-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib-unix.h>
#include <glib.h>

#include <g3k.h>

#define G3K_TYPE_EXAMPLE g3k_example_get_type ()
G_DECLARE_FINAL_TYPE (G3kExample, g3k_example, G3K, EXAMPLE, GObject)

struct _G3kExample
{
  GObject parent;

  G3kContext    *g3k;
  G3kLoader     *loader;
  G3kBackground *background;
  GMainLoop     *loop;
};

G_DEFINE_TYPE (G3kExample, g3k_example, G_TYPE_OBJECT)

static void
_finalize (GObject *gobject);

static void
g3k_example_class_init (G3kExampleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

static void
_finalize (GObject *gobject)
{
  G3kExample *self = G3K_EXAMPLE (gobject);

  g_object_unref (self->background);
  g_object_unref (self->loader);
  g_clear_object (&self->g3k);
  G_OBJECT_CLASS (g3k_example_parent_class)->finalize (gobject);
}

static void
g3k_example_init (G3kExample *self)
{
  self->g3k = NULL;
  self->background = NULL;
  self->loop = g_main_loop_new (NULL, FALSE);
}

static void
_shutdown_cb (GxrContext *context, gpointer _self)
{
  (void) context;
  G3kExample *self = G3K_EXAMPLE (_self);
  g_main_loop_quit (self->loop);
}

static void
_init_input_callbacks (G3kExample *self)
{
  g_signal_connect (self->g3k, "shutdown", G_CALLBACK (_shutdown_cb), self);
}

static gboolean
_sigint_cb (gpointer _self)
{
  G3kExample *self = (G3kExample *) _self;
  g_main_loop_quit (self->loop);
  return TRUE;
}

static G3kExample *
g3k_example_new (void)
{
  G3kExample *self = (G3kExample *) g_object_new (G3K_TYPE_EXAMPLE, 0);

  GSList *device_ext_list = NULL;
  device_ext_list
    = g_slist_append (device_ext_list,
                      VK_KHR_SAMPLER_MIRROR_CLAMP_TO_EDGE_EXTENSION_NAME);

  self->g3k = g3k_context_new_full (NULL, device_ext_list, "GLTF Example",
                                    G3K_VERSION_HEX);
  if (!self->g3k)
    {
      g_printerr ("Could not init VR runtime.\n");
      return NULL;
    }

  self->background = g3k_background_new (self->g3k);
  g3k_object_add_child (g3k_context_get_root (self->g3k),
                        G3K_OBJECT (self->background), 0);

  _init_input_callbacks (self);

  g_unix_signal_add (SIGINT, _sigint_cb, self);

  return self;
}

static gboolean
_render_cb (G3kExample *self)
{
  g3k_context_render (self->g3k);
  return true;
}

static gboolean
_init_model (G3kExample *self, const char *model_path, float scale)
{
  graphene_matrix_t m;
  graphene_matrix_init_scale (&m, scale, scale, scale);

  GError *model_error = NULL;
  self->loader = g3k_loader_new_from_file (self->g3k, model_path, &model_error);

  if (!self->loader || model_error != NULL)
    {
      g_warning ("Could not load GLTF model from %s: %s\n", model_path,
                 model_error->message);
      g_error_free (model_error);
      return FALSE;
    }

  G3kObject *model_root = g3k_loader_get_root (self->loader);
  g3k_object_set_matrix (model_root, &m);
  g3k_object_add_child (g3k_context_get_root (self->g3k), model_root, 0);

  return TRUE;
}

static void
print_usage (void)
{
  g_print ("Usage:\n");
  g_print ("./load_gltf <model_path> <?scale>\n");
}

int
main (int argc, char *argv[])
{
  G3kExample *example = g3k_example_new ();
  if (argc != 2 && argc != 3)
    {
      print_usage ();
      return EXIT_FAILURE;
    }

  char *model_path = argv[1];
  if (!g_file_test (model_path, G_FILE_TEST_IS_REGULAR))
    {
      g_warning ("%s is not a path to a valid file.\n", model_path);
      return EXIT_FAILURE;
    }

  float scale = 1.0f;
  if (argc == 3)
    {
      scale = (float) atof (argv[2]);
    }

  g_print ("Loading model from %s\n", model_path);

  if (!_init_model (example, model_path, scale))
    return EXIT_FAILURE;

  g_timeout_add (1, (GSourceFunc) _render_cb, example);

  /* start glib main loop */
  g_main_loop_run (example->loop);
  g_main_loop_unref (example->loop);

  g_object_unref (example);

  return EXIT_SUCCESS;
}
