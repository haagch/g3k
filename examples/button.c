/*
 * xrdesktop
 * Copyright 2019-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib-unix.h>
#include <glib.h>

#include <g3k.h>

#define BUTTON_TYPE_EXAMPLE button_example_get_type ()
G_DECLARE_FINAL_TYPE (ButtonExample, button_example, BUTTON, EXAMPLE, GObject)

struct _ButtonExample
{
  GObject parent;

  GxrActionSet *action_set;

  G3kContext *g3k;

  G3kBackground *background;

  GxrManifest *manifest;

  GMainLoop *loop;
  G3kButton *quit_button;
};

G_DEFINE_TYPE (ButtonExample, button_example, G_TYPE_OBJECT)

static void
_finalize (GObject *gobject);

static void
button_example_class_init (ButtonExampleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

static void
_finalize (GObject *gobject)
{
  ButtonExample *self = BUTTON_EXAMPLE (gobject);

  g_object_unref (self->background);

  g_clear_object (&self->action_set);
  g_clear_object (&self->manifest);
  g_clear_object (&self->g3k);
  G_OBJECT_CLASS (button_example_parent_class)->finalize (gobject);
}

static gboolean
_is_hovered (ButtonExample *self, G3kButton *button)
{
  g_autoptr (GList) controllers = g3k_context_get_controllers (self->g3k);
  for (GList *l = controllers; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);
      if (g3k_controller_get_hover_state (controller)->object
          == G3K_OBJECT (button))
        return TRUE;
    }
  return FALSE;
}

static void
_check_hover_grab (ButtonExample *self)
{
  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g_autoptr (GList) controllers = g3k_context_get_controllers (self->g3k);
  for (GList *l = controllers; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);

      /* TODO: handle invalid pointer poses better */
      if (!gxr_controller_is_pointer_pose_valid (
            g3k_controller_get_controller (controller)))
        continue;

      g3k_object_manager_update_controller (manager, controller);
    }
}

static void
_input_poll_cb (G3kContext *context, gpointer _self)
{
  (void) context;
  ButtonExample *self = BUTTON_EXAMPLE (_self);

  if (self->action_set == NULL)
    {
      g_printerr ("Error: Action Set not created!\n");
      return;
    }

  if (!gxr_action_sets_poll (&self->action_set, 1))
    {
      g_printerr ("Error polling actions\n");
      return;
    }

  _check_hover_grab (self);
}

static void
_action_grab_cb (GxrAction *action, GxrDigitalEvent *event, ButtonExample *self)
{
  (void) action;
  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);

  if (event->changed)
    {
      if (event->state == 1)
        g3k_controller_check_grab (controller);
      else
        g3k_controller_check_release (controller);
    }
}

static void
_button_hover_cb (G3kButton *button, G3kHoverEvent *event, gpointer _self)
{
  (void) _self;
  (void) event;
  g3k_button_set_selection_color (button, TRUE);
}

static void
_button_hover_end_cb (G3kButton     *button,
                      GxrController *controller,
                      gpointer       _self)
{
  (void) controller;
  ButtonExample *self = BUTTON_EXAMPLE (_self);
  // ButtonExamplePrivate *priv = button_example_get_instance_private (self);

  /* unmark if no controller is hovering over this button */
  if (!_is_hovered (self, button))
    g3k_button_reset_color (button);
}

static void
button_example_init (ButtonExample *self)
{
  self->action_set = NULL;
  self->g3k = NULL;
  self->background = NULL;
  self->loop = g_main_loop_new (NULL, FALSE);
  self->manifest = NULL;
}

static void
_shutdown_cb (GxrContext *context, gpointer _self)
{
  (void) context;
  ButtonExample *self = BUTTON_EXAMPLE (_self);
  g_main_loop_quit (self->loop);
}

static GxrActionSet *
_create_wm_action_set (ButtonExample *self)
{
  GxrActionSet *set
    = gxr_action_set_new_from_url (g3k_context_get_gxr (self->g3k),
                                   self->manifest, "/actions/wm");

  GxrDeviceManager *dm
    = gxr_context_get_device_manager (g3k_context_get_gxr (self->g3k));
  gxr_device_manager_connect_pose_actions (dm, set, "/actions/wm/in/hand_pose",
                                           "/actions/wm/in/"
                                           "hand_pose_hand_grip");
  gxr_action_set_connect_digital_from_float (set, "/actions/wm/in/grab_window",
                                             0.25f, "/actions/wm/out/haptic",
                                             (GCallback) _action_grab_cb, self);
  return set;
}

static void
_init_input_callbacks (ButtonExample *self)
{
  self->manifest = gxr_manifest_new ("/res/bindings", "wm_actions.json");
  if (!self->manifest)
    {
      g_print ("Failed to load action bindings!\n");
      return;
    }

  self->action_set = _create_wm_action_set (self);

  gxr_context_attach_action_sets (g3k_context_get_gxr (self->g3k),
                                  &self->action_set, 1);

  g_signal_connect (self->g3k, "input-poll", G_CALLBACK (_input_poll_cb), self);
  g_signal_connect (self->g3k, "shutdown", G_CALLBACK (_shutdown_cb), self);
}

static gboolean
_sigint_cb (gpointer _self)
{
  ButtonExample *self = (ButtonExample *) _self;
  g_main_loop_quit (self->loop);
  return TRUE;
}

static void
_button_quit_press_cb (G3kObject     *object,
                       GxrController *controller,
                       gpointer       _self)
{
  (void) controller;
  (void) object;
  ButtonExample *self = _self;
  g_main_loop_quit (self->loop);
}

static void
_init_buttons (ButtonExample *self)
{
  graphene_point3d_t button_pos = {.x = -2.5f, .y = 0.5f, .z = -2.5f};
  graphene_size_t    size_meters = {.6f, .6f};

  gchar *quit_str[] = {"Quit"};
  self->quit_button = g3k_button_new (self->g3k, &size_meters);
  if (!self->quit_button)
    return;

  g3k_button_set_text (self->quit_button, 1, quit_str);

  graphene_matrix_t transform;
  graphene_matrix_init_translate (&transform, &button_pos);
  g3k_object_set_matrix (G3K_OBJECT (self->quit_button), &transform);

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_add_object (manager, G3K_OBJECT (self->quit_button),
                                 G3K_INTERACTION_HOVERABLE
                                   | G3K_INTERACTION_DESTROY_WITH_PARENT
                                   | G3K_INTERACTION_BUTTON);

  g_signal_connect (self->quit_button, "grab-start-event",
                    (GCallback) _button_quit_press_cb, self);

  g_signal_connect (self->quit_button, "hover-event",
                    (GCallback) _button_hover_cb, self);

  g_signal_connect (self->quit_button, "hover-end-event",
                    (GCallback) _button_hover_end_cb, self);

  g3k_object_add_child (g3k_context_get_root (self->g3k),
                        G3K_OBJECT (self->quit_button), 0);
}

static ButtonExample *
button_example_new (void)
{
  G3kContext *g3k = g3k_context_new_full (NULL, NULL, "button example",
                                          G3K_VERSION_HEX);
  if (!g3k)
    {
      g_printerr ("Could not init VR runtime.\n");
      return NULL;
    }

  ButtonExample *self = (ButtonExample *) g_object_new (BUTTON_TYPE_EXAMPLE, 0);

  self->g3k = g3k;

  self->background = g3k_background_new (self->g3k);
  g3k_object_add_child (g3k_context_get_root (self->g3k),
                        G3K_OBJECT (self->background), 0);

  _init_input_callbacks (self);

  _init_buttons (self);

  g_unix_signal_add (SIGINT, _sigint_cb, self);

  return self;
}

static gboolean
_render_cb (ButtonExample *self)
{
  g3k_context_render (self->g3k);
  return true;
}

int
main ()
{
  ButtonExample *example = button_example_new ();

  g_timeout_add (1, (GSourceFunc) _render_cb, example);

  g_main_loop_run (example->loop);
  g_main_loop_unref (example->loop);

  g_object_unref (example);

  return EXIT_SUCCESS;
}
