/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-settings.h"

static GHashTable *instances = NULL;

gboolean
g3k_settings_is_schema_installed (const gchar *schema_id)
{
  GSettingsSchemaSource *source = g_settings_schema_source_get_default ();
  GSettingsSchema *schema = g_settings_schema_source_lookup (source, schema_id,
                                                             TRUE);

  gboolean installed = schema != NULL;

  if (schema)
    g_settings_schema_unref (schema);

  return installed;
}

/**
 * g3k_settings_get_instance:
 *
 * Returns: (transfer none): The #GSettings for xrdesktop
 */
GSettings *
g3k_settings_get_instance (const gchar *schema_id)
{
  if (!instances)
    {
      instances = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                         g_object_unref);
    }

  GSettings *settings = NULL;
  if (g_hash_table_contains (instances, schema_id))
    {
      settings = G_SETTINGS (g_hash_table_lookup (instances, schema_id));
    }
  else
    {
      settings = g_settings_new (schema_id);
      g_hash_table_insert (instances, g_strdup (schema_id), settings);
    }

  return settings;
}

void
g3k_settings_destroy ()
{
  if (instances)
    {
      g_hash_table_destroy (instances);
    }
  instances = NULL;
}

typedef void (*settings_callback) (GSettings *settings,
                                   gchar     *key,
                                   gpointer   user_data);

/**
 * g3k_settings_connect_and_apply:
 * @callback: (scope async): A function that will be called with the given
 * @key and @data 1) immediately and 2) when the value for the given key is
 * updated.
 * @key: The settings key
 * @data: A pointer that will be passed to the update callback.
 *
 * Use this convenience function when you don't want to initially read a config
 * value from the settings, and then connect a callback to when the value
 * changes.
 *
 * Instead write only one callback that handles initially setting the value, as
 * well as any updates to this value.
 */
void
g3k_settings_connect_and_apply (GCallback    callback,
                                const gchar *schema_id,
                                gchar       *key,
                                gpointer     data)
{
  GSettings *settings = g3k_settings_get_instance (schema_id);

  settings_callback cb = (settings_callback) callback;
  cb (settings, key, data);

  GString *detailed_signal = g_string_new ("changed::");
  g_string_append (detailed_signal, key);

  g_signal_connect (settings, detailed_signal->str, callback, data);

  g_string_free (detailed_signal, TRUE);
}

/**
 * g3k_settings_update_double_val:
 * @settings: The gsettings
 * @key: The key
 * @val: (out): Pointer to a double value to be updated
 *
 * Convencience callback that can be registered when no action is required on
 * an update of a double value.
 */
void
g3k_settings_update_double_val (GSettings *settings, gchar *key, double *val)
{
  *val = g_settings_get_double (settings, key);
}

/**
 * g3k_settings_update_int_val:
 * @settings: The gsettings
 * @key: The key
 * @val: (out): Pointer to an int value to be updated
 *
 * Convencience callback that can be registered when no action is required on
 * an update of a int value.
 */
void
g3k_settings_update_int_val (GSettings *settings, gchar *key, int *val)
{
  *val = g_settings_get_int (settings, key);
}

/**
 * g3k_settings_update_gboolean_val:
 * @settings: The gsettings
 * @key: The key
 * @val: (out): Pointer to an gboolean value to be updated
 *
 * Convencience callback that can be registered when no action is required on
 * an update of a gboolean value.
 */
void
g3k_settings_update_gboolean_val (GSettings *settings,
                                  gchar     *key,
                                  gboolean  *val)
{
  *val = g_settings_get_boolean (settings, key);
}
