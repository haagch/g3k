/*
 * xrdesktop
 * Copyright 2022 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_PPM_H_
#define G3K_PPM_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib.h>
#include <graphene.h>
#include <stdint.h>
#include <vulkan/vulkan.h>

static inline float
g3k_to_meters (int32_t pixel, float ppm)
{
  return (float) pixel / ppm;
}

static inline int32_t
g3k_to_pixels (float meter, float ppm)
{
  return (int32_t) (ppm * meter);
}

static inline uint32_t
g3k_to_upixels (float meter, float ppm)
{
  return (uint32_t) (ppm * meter);
}

static inline float
g3k_to_ppm (int32_t pixel, float meter)
{
  return (float) pixel / meter;
}

static inline VkExtent2D
g3k_size_to_extent (graphene_size_t *size_meters, float ppm)
{
  VkExtent2D extent = {
    .width = g3k_to_upixels (size_meters->width, ppm),
    .height = g3k_to_upixels (size_meters->height, ppm),
  };

  return extent;
}

static inline graphene_size_t
g3k_extent_to_size (VkExtent2D *extent, float ppm)
{
  graphene_size_t size = {
    .width = g3k_to_meters ((int32_t) extent->width, ppm),
    .height = g3k_to_meters ((int32_t) extent->height, ppm),
  };

  return size;
}

#endif
