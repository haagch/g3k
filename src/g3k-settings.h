/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_SETTINGS_H_
#define G3K_SETTINGS_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <gio/gio.h>

gboolean
g3k_settings_is_schema_installed (const gchar *schema_id);

GSettings *
g3k_settings_get_instance (const gchar *schema_id);

void
g3k_settings_destroy (void);

void
g3k_settings_connect_and_apply (GCallback    callback,
                                const gchar *schema_id,
                                gchar       *key,
                                gpointer     data);

void
g3k_settings_update_double_val (GSettings *settings, gchar *key, double *val);
void
g3k_settings_update_int_val (GSettings *settings, gchar *key, int *val);
void
g3k_settings_update_gboolean_val (GSettings *settings,
                                  gchar     *key,
                                  gboolean  *val);

#endif /* G3K_SETTINGS_H_ */
