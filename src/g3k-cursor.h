/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_CURSOR_H_
#define G3K_CURSOR_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>
#include <graphene.h>

#include "g3k-plane.h"

G_BEGIN_DECLS

#define G3K_TYPE_CURSOR g3k_cursor_get_type ()
G_DECLARE_FINAL_TYPE (G3kCursor, g3k_cursor, G3K, CURSOR, G3kPlane)

G3kCursor *
g3k_cursor_new (G3kContext *g3k);

void
g3k_cursor_set_hotspot (G3kCursor *self, int hotspot_x, int hotspot_y);

void
g3k_cursor_update (G3kCursor          *self,
                   G3kObject          *target,
                   graphene_point3d_t *intersection);

G_END_DECLS

#endif /* G3K_CURSOR_H_ */
