/*
 * xrdesktop
 * Copyright 2020 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_TYPES_H_
#define G3K_TYPES_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-controller.h"

/**
 * G3kHoverEvent:
 * @point: The point in 3D world space.
 * @pose: A #graphene_matrix_t pose.
 * @distance: Distance from the controller.
 * @controller: The controller the event was captured on.
 *
 * An event that gets emitted when a controller hovers a window.
 **/
typedef struct
{
  graphene_point3d_t point;
  graphene_matrix_t  pose;
  float              distance;
  G3kController     *controller;
} G3kHoverEvent;

/**
 * G3kGrabEvent:
 * @pose: A #graphene_matrix_t pose.
 * @controller: The controller the event was captured on.
 *
 * An event that gets emitted when a window get grabbed.
 **/
typedef struct
{
  G3kController *controller;
} G3kGrabEvent;

typedef struct
{
  const gchar *string;
  const guint  keyval;
} G3kKeyEvent;

typedef enum
{
  G3K_RENDER_EVENT_FRAME_START,
  G3K_RENDER_EVENT_FRAME_END,
} G3kRenderEventType;

typedef struct
{
  G3kRenderEventType type;
} G3kRenderEvent;

#endif /* G3K_TYPES_H_ */
