/*
 * xrdesktop
 * Copyright 2018-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Manas Chaudhary <manaschaudhary2000@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_MESH_H_
#define G3K_MESH_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-attribute.h"
#include "g3k-material.h"
#include "g3k-pipeline.h"

G_BEGIN_DECLS

struct _G3kPrimitive
{
  GObject parent;

  G3kContext *context;

  GHashTable         *accessors;
  G3kAccessor        *index_accessor;
  gsize               index_count;
  VkIndexType         index_type;
  G3kMaterial        *material;
  GulkanVertexBuffer *vertex_buffer;

  G3kPipeline *pipeline;
};

#define G3K_TYPE_PRIMITIVE g3k_primitive_get_type ()
G_DECLARE_FINAL_TYPE (G3kPrimitive, g3k_primitive, G3K, PRIMITIVE, GObject)

struct _G3kMesh
{
  GObject parent;

  char      *name;
  GPtrArray *primitives;
};

#define G3K_TYPE_MESH g3k_mesh_get_type ()
G_DECLARE_FINAL_TYPE (G3kMesh, g3k_mesh, G3K, MESH, GObject)

G3kMesh *
g3k_mesh_new (void);

G3kPrimitive *
g3k_primitive_new (G3kContext *context);

bool
g3k_primitive_add_index (G3kPrimitive *self, G3kAccessor *accessor);

bool
g3k_primitive_add_attribute (G3kPrimitive *self,
                             const char   *name,
                             G3kAccessor  *accessor);

char *
g3k_primitive_get_attribute_config_key (G3kPrimitive *self);

gboolean
g3k_primitive_init_vertex_buffer (G3kPrimitive *self, G3kContext *g3k);

VkVertexInputAttributeDescription *
g3k_primitive_create_attrib_desc (G3kPrimitive *self);

char *
g3k_primitive_get_attrib_defines (G3kPrimitive *self);

void
g3k_primitive_set_pipeline (G3kPrimitive *self, G3kPipeline *pipeline);

G3kPipeline *
g3k_primitive_create_pipeline (G3kPrimitive *self);

char *
g3k_primitive_get_pipeline_key (G3kPrimitive *self);

void
g3k_primitive_draw (G3kPrimitive        *self,
                    GulkanDescriptorSet *descriptor_set,
                    VkCommandBuffer      cmd_buffer);

G_END_DECLS

#endif /* G3K_MESH_H_ */
