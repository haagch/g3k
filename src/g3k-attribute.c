/*
 * xrdesktop
 * Copyright 2021-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-attribute.h"

G3kAttributeArray *
g3k_attribute_array_new (G3kAttributeType type, gsize count, gsize stride)
{
  G3kAttributeArray *array = g_malloc0 (sizeof (G3kAttributeArray));
  array->ref_count = 1;
  array->type = type;
  array->count = count;
  array->stride = stride;

  // gsize size = g3k_attribute_type_size (type) * count * stride;
  // array->data = g_malloc0 (size * 3);

  return array;
}

G3kAttributeArray *
g3k_attribute_array_ref (G3kAttributeArray *array)
{
  g_assert (array->ref_count > 0);
  array->ref_count++;
  return array;
}

G3kAttributeType
g3k_attribute_type_from_component_type (gint64 component_type)
{
  switch (component_type)
    {
      case 5120: // BYTE
        return G3K_ATTRIBUTE_TYPE_INT8;
      case 5121: // UNSIGNED_BYTE
        return G3K_ATTRIBUTE_TYPE_UINT8;
      case 5122: // SHORT
        return G3K_ATTRIBUTE_TYPE_INT16;
      case 5123: // UNSIGNED_SHORT
        return G3K_ATTRIBUTE_TYPE_UINT16;
      case 5125: // UNSIGNED_INT
        return G3K_ATTRIBUTE_TYPE_UINT32;
      case 5126: // FLOAT
        return G3K_ATTRIBUTE_TYPE_FLOAT;
      default:
        // Unreachable
        g_error ("Unknown accessor component type %ld", component_type);
        return 0;
    }
}

void
g3k_attribute_array_unref (G3kAttributeArray *array)
{
  g_assert (array->ref_count > 0);
  array->ref_count--;

  if (array->ref_count == 0)
    {
      // g_free (array->data);
      g_free (array);
    }
}

gsize
g3k_attribute_type_size (G3kAttributeType type)
{
  switch (type)
    {
      case G3K_ATTRIBUTE_TYPE_DOUBLE:
        return 8;
      case G3K_ATTRIBUTE_TYPE_FLOAT:
        return 4;
      case G3K_ATTRIBUTE_TYPE_UINT32:
        return 4;
      case G3K_ATTRIBUTE_TYPE_INT32:
        return 4;
      case G3K_ATTRIBUTE_TYPE_UINT16:
        return 2;
      case G3K_ATTRIBUTE_TYPE_INT16:
        return 2;
      case G3K_ATTRIBUTE_TYPE_UINT8:
        return 1;
      case G3K_ATTRIBUTE_TYPE_INT8:
        return 1;
      default:
        // Unreachable
        g_error ("Unknown size for attribute type %d", type);
        return 0;
    }
}

#define ENUM_TO_STR(r)                                                         \
  case r:                                                                      \
    return #r

const gchar *
g3k_attribute_type_string (G3kAttributeType type)
{
  switch (type)
    {
      ENUM_TO_STR (G3K_ATTRIBUTE_TYPE_DOUBLE);
      ENUM_TO_STR (G3K_ATTRIBUTE_TYPE_FLOAT);
      ENUM_TO_STR (G3K_ATTRIBUTE_TYPE_UINT32);
      ENUM_TO_STR (G3K_ATTRIBUTE_TYPE_INT32);
      ENUM_TO_STR (G3K_ATTRIBUTE_TYPE_UINT16);
      ENUM_TO_STR (G3K_ATTRIBUTE_TYPE_INT16);
      ENUM_TO_STR (G3K_ATTRIBUTE_TYPE_UINT8);
      ENUM_TO_STR (G3K_ATTRIBUTE_TYPE_INT8);
      default:
        return "UNKNOWN TYPE";
    }
}
