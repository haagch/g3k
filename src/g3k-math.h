/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_MATH_H_
#define G3K_MATH_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif

#include <glib.h>
#include <graphene.h>

void
g3k_math_get_rotation_angles (graphene_vec3_t *direction,
                              float           *azimuth,
                              float           *inclination);

void
g3k_math_matrix_set_translation_point (graphene_matrix_t  *matrix,
                                       graphene_point3d_t *point);

gboolean
g3k_math_intersect_lines_2d (graphene_point_t *p0,
                             graphene_point_t *p1,
                             graphene_point_t *p2,
                             graphene_point_t *p3,
                             graphene_point_t *intersection);

gboolean
g3k_math_clamp_towards_zero_2d (graphene_point_t *min,
                                graphene_point_t *max,
                                graphene_point_t *point,
                                graphene_point_t *clamped);

void
g3k_math_sphere_to_3d_coords (float               azimuth,
                              float               inclination,
                              float               distance,
                              graphene_point3d_t *point);

#endif /* G3K_MATH_H_ */
