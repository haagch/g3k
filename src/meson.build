so_version = 0
api_version = '0.16'
api_path = 'g3k-' + api_version

sources = [
  'g3k-settings.c',
  'g3k-math.c',
  'g3k-object-manager.c',
  'g3k-cursor.c',
  'g3k-button.c',
  'g3k-container.c',
  'g3k-keyboard.c',
  'g3k-ray.c',
  'g3k-object.c',
  'g3k-selection.c',
  'g3k-background.c',
  'g3k-model.c',
  'g3k-mesh.c',
  'g3k-loader.c',
  'g3k-tip.c',
  'g3k-renderer.c',
  'g3k-controller.c',
  'g3k-context.c',
  'g3k-plane.c',
  'g3k-attribute.c',
  'g3k-pose.c',
  'g3k-keyboard-button.c',
  'g3k-pipeline.c',
  'g3k-material.c',
  'g3k-sampler.c',
  binding_resources[0],
  icon_resources[0],
  keyboard_layout_resources[0],
  shader_resources[0]
]

headers = [
  'g3k.h',
  'g3k-settings.h',
  'g3k-math.h',
  'g3k-object-manager.h',
  'g3k-types.h',
  'g3k-cursor.h',
  'g3k-button.h',
  'g3k-container.h',
  'g3k-keyboard.h',
  'g3k-ray.h',
  'g3k-object.h',
  'g3k-selection.h',
  'g3k-background.h',
  'g3k-ppm.h',
  'g3k-model.h',
  'g3k-mesh.h',
  'g3k-loader.h',
  'g3k-tip.h',
  'g3k-renderer.h',
  'g3k-controller.h',
  'g3k-context.h',
  'g3k-plane.h',
  'g3k-attribute.h',
  'g3k-pose.h',
  'g3k-keyboard-button.h',
  'g3k-pipeline.h',
  'g3k-material.h',
  'g3k-sampler.h',
]

version_split = meson.project_version().split('.')
MAJOR_VERSION = version_split[0]
MINOR_VERSION = version_split[1]
MICRO_VERSION = version_split[2]

version_conf = configuration_data()
version_conf.set('VERSION', meson.project_version())
version_conf.set('MAJOR_VERSION', MAJOR_VERSION)
version_conf.set('MINOR_VERSION', MINOR_VERSION)
version_conf.set('MICRO_VERSION', MICRO_VERSION)

configure_file(
  input: 'g3k-version.h.in',
  output: 'g3k-version.h',
  configuration: version_conf,
  install_dir: join_paths(get_option('includedir'), api_path)
)

deps = [
  m_dep,
  gxr_dep,
  json_glib_dep,
  pango_dep,
  canberra_dep
] + shaderc_deps

inc = include_directories('.')

c_args = ['-DG3K_COMPILATION']

if get_option('buildtype') == 'debug'
  c_args += '-DVALIDATE_TRANSFORM'
endif

renderdoc = get_option('renderdoc')
if renderdoc
  c_args += '-DRENDERDOC'
  dl_dep = meson.get_compiler('c').find_library('dl', required : true)
  deps += dl_dep
endif

g3k_lib = shared_library(api_path,
  sources,
  version: meson.project_version(),
  soversion: so_version,
  dependencies: deps,
  include_directories: inc,
  install: true,
  c_args : c_args
)

g3k_dep = declare_dependency(
  sources: [],
  link_with: g3k_lib,
  include_directories: inc,
  dependencies: deps,
)

install_headers(headers, subdir: api_path)

pkg = import('pkgconfig')

gxr_version_array = gxr_dep.version().split('.')
gxr_api_version = '.'.join([gxr_version_array[0], gxr_version_array[1]])

pkg.generate(
  description: 'A 3DUI widget toolkit.',
    libraries: g3k_lib,
         name: 'g3k',
     filebase: api_path,
      version: meson.project_version(),
      subdirs: api_path,
     requires: 'gxr-' + gxr_api_version,
  install_dir: join_paths(get_option('libdir'), 'pkgconfig')
)

if get_option('introspection')
  xrdesktop_gir = gnome.generate_gir(
    g3k_lib,
    sources: sources + headers,
    namespace: 'G3k',
    nsversion: api_version,
    identifier_prefix: 'G3k',
    symbol_prefix: 'g3k',
    export_packages: api_path,
    includes: [ 'cairo-1.0', 'GdkPixbuf-2.0', 'Gdk-3.0', 'Graphene-1.0', 'Json-1.0', 'Gulkan-' + api_version, 'Gxr-' + api_version],
    header: 'g3k.h',
    install: true,
  )
endif
