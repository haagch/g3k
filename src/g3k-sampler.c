/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-sampler.h"

G_DEFINE_TYPE (G3kSampler, g3k_sampler, G_TYPE_OBJECT)

static void
_finalize (GObject *gobject)
{
  G3kSampler *self = G3K_SAMPLER (gobject);
  (void) self;
  G_OBJECT_CLASS (g3k_sampler_parent_class)->finalize (gobject);
}

static void
g3k_sampler_class_init (G3kSamplerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

G3kSampler *
g3k_sampler_new ()
{
  G3kSampler *self = (G3kSampler *) g_object_new (G3K_TYPE_SAMPLER, 0);
  return self;
}

static void
g3k_sampler_init (G3kSampler *self)
{
  self->mag_filter = VK_FILTER_LINEAR;
  self->min_filter = VK_FILTER_LINEAR;
  self->wrap_s = VK_SAMPLER_ADDRESS_MODE_REPEAT;
  self->wrap_t = VK_SAMPLER_ADDRESS_MODE_REPEAT;
  self->mipmap_mode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
}
