/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_PLANE_PRIV_H_
#define G3K_PLANE_PRIV_H_

#include "g3k-plane.h"

void
g3k_plane_update_mesh (G3kPlane *self);

void
g3k_plane_store_texture (G3kPlane *self, GulkanTexture *texture);

#endif
