/*
 * xrdesktop
 * Copyright 2021-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_ATTRIBUTE_H_
#define G3K_ATTRIBUTE_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

typedef enum
{
  G3K_ATTRIBUTE_TYPE_DOUBLE,
  G3K_ATTRIBUTE_TYPE_FLOAT,
  G3K_ATTRIBUTE_TYPE_UINT32,
  G3K_ATTRIBUTE_TYPE_INT32,
  G3K_ATTRIBUTE_TYPE_UINT16,
  G3K_ATTRIBUTE_TYPE_INT16,
  G3K_ATTRIBUTE_TYPE_UINT8,
  G3K_ATTRIBUTE_TYPE_INT8,
} G3kAttributeType;

typedef struct
{
  int              ref_count;
  G3kAttributeType type;
  gsize            stride; /* in nr of type items */
  gsize            count;  /* in nr of stride items */
  int              version;
  GBytes          *bytes;
} G3kAttributeArray;

typedef struct
{
  G3kAttributeArray *array;
  gboolean           normalized;
  gsize              item_size;   // in array->type size units
  gsize              item_offset; // in array->type size units
  gsize              count;
} G3kAccessor;

void
g3k_attribute_array_unref (G3kAttributeArray *array);

G3kAttributeArray *
g3k_attribute_array_new (G3kAttributeType type, gsize count, gsize stride);

G3kAttributeArray *
g3k_attribute_array_ref (G3kAttributeArray *array);

G3kAttributeType
g3k_attribute_type_from_component_type (gint64 component_type);

void
g3k_attribute_array_unref (G3kAttributeArray *array);

gsize
g3k_attribute_type_size (G3kAttributeType type);

const gchar *
g3k_attribute_type_string (G3kAttributeType type);

G_END_DECLS

#endif /* G3K_ATTRIBUTE_H_ */
