/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-selection.h"

#include <stdalign.h>

#include "g3k-renderer.h"

typedef struct
{
  alignas (32) float mvp[2][16];
} G3kSelectionUniformBuffer;

struct _G3kSelection
{
  G3kObject           parent;
  GulkanVertexBuffer *vertex_buffer;
};

G_DEFINE_TYPE (G3kSelection, g3k_selection, G3K_TYPE_OBJECT)

static void
g3k_selection_finalize (GObject *gobject);

static void
_draw (G3kObject *obj, VkCommandBuffer cmd_buffer);

static void
g3k_selection_class_init (G3kSelectionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = g3k_selection_finalize;

  G3kObjectClass *g3k_object_class = G3K_OBJECT_CLASS (klass);
  g3k_object_class->draw = _draw;
}

static void
g3k_selection_init (G3kSelection *self)
{
  G3kObject *obj = G3K_OBJECT (self);
  g3k_object_set_visibility (obj, FALSE);
}

gboolean
_initialize (G3kSelection *self, G3kContext *g3k);

G3kSelection *
g3k_selection_new (G3kContext *g3k)
{
  G3kSelection *self = (G3kSelection *) g_object_new (G3K_TYPE_SELECTION, 0);
  _initialize (self, g3k);
  return self;
}

static void
g3k_selection_finalize (GObject *gobject)
{
  G3kSelection *self = G3K_SELECTION (gobject);
  g_object_unref (self->vertex_buffer);
  G_OBJECT_CLASS (g3k_selection_parent_class)->finalize (gobject);
}

static void
_append_lines_quad (GulkanVertexBuffer *self,
                    float               width,
                    float               height,
                    graphene_vec3_t    *color)
{
  float scale_x = width;
  float scale_y = height;

  graphene_vec4_t a, b, c, d;
  // clang-format off
  graphene_vec4_init (&a, -scale_x/2.0f, -scale_y / 2, 0, 1);
  graphene_vec4_init (&b,  scale_x/2.0f, -scale_y / 2, 0, 1);
  graphene_vec4_init (&c,  scale_x/2.0f,  scale_y / 2, 0, 1);
  graphene_vec4_init (&d, -scale_x/2.0f,  scale_y / 2, 0, 1);
  // clang-format on

  graphene_vec4_t points[8] = {
    a, b, b, c, c, d, d, a,
  };

  for (uint32_t i = 0; i < G_N_ELEMENTS (points); i++)
    {
      gulkan_vertex_buffer_append_with_color (self, &points[i], color);
    }
}

void
g3k_selection_set_quad (G3kSelection *self, float width, float height)
{
  gulkan_vertex_buffer_reset (self->vertex_buffer);

  graphene_vec3_t color;
  graphene_vec3_init (&color, .078f, .471f, .675f);

  _append_lines_quad (self->vertex_buffer, width, height, &color);

  gulkan_vertex_buffer_map_array (self->vertex_buffer);
}

gboolean
_initialize (G3kSelection *self, G3kContext *g3k)
{
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);
  self->vertex_buffer
    = gulkan_vertex_buffer_new (device, VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
  gulkan_vertex_buffer_reset (self->vertex_buffer);

  graphene_vec3_t color;
  graphene_vec3_init (&color, .078f, .471f, .675f);

  _append_lines_quad (self->vertex_buffer, 0.0f, 0.0f, &color);

  if (!gulkan_vertex_buffer_alloc_empty (self->vertex_buffer,
                                         GXR_DEVICE_INDEX_MAX))
    return FALSE;

  gulkan_vertex_buffer_map_array (self->vertex_buffer);

  G3kObject *obj = G3K_OBJECT (self);

  VkDeviceSize ub_size = sizeof (G3kSelectionUniformBuffer);

  G3kRenderer *renderer = g3k_context_get_renderer (g3k);
  G3kPipeline *pipeline = g3k_renderer_get_pipeline (renderer, "selection");

  if (!g3k_object_initialize (obj, g3k, pipeline, ub_size))
    return FALSE;

  return TRUE;
}

static void
_update_ubo (G3kSelection *self)
{
  G3kSelectionUniformBuffer ub = {0};

  graphene_matrix_t m_matrix;
  g3k_object_get_matrix (G3K_OBJECT (self), &m_matrix);

  G3kContext        *context = g3k_object_get_context (G3K_OBJECT (self));
  graphene_matrix_t *vp = g3k_context_get_vps (context);

  for (uint32_t eye = 0; eye < 2; eye++)
    {
      graphene_matrix_t mvp_matrix;
      graphene_matrix_multiply (&m_matrix, &vp[eye], &mvp_matrix);
      graphene_matrix_to_float (&mvp_matrix, ub.mvp[eye]);
    }

  g3k_object_update_transformation_ubo (G3K_OBJECT (self), &ub);
}

static void
_draw (G3kObject *obj, VkCommandBuffer cmd_buffer)
{
  G3kSelection *self = G3K_SELECTION (obj);

  if (!gulkan_vertex_buffer_is_initialized (self->vertex_buffer))
    return;

  _update_ubo (self);

  g3k_object_bind (obj, cmd_buffer);
  gulkan_vertex_buffer_draw (self->vertex_buffer, cmd_buffer);
}
