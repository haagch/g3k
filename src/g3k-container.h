/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_CONTAINER_H_
#define G3K_CONTAINER_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>

#include "g3k-object.h"

G_BEGIN_DECLS

#define G3K_TYPE_CONTAINER g3k_container_get_type ()
G_DECLARE_FINAL_TYPE (G3kContainer, g3k_container, G3K, CONTAINER, G3kObject)

/**
 * G3kContainerAttachment:
 * @G3K_CONTAINER_ATTACHMENT_NONE: The #G3kContainer is not attached.
 * @G3K_CONTAINER_ATTACHMENT_HEAD: The container is tracking the head.
 * @G3K_CONTAINER_ATTACHMENT_HAND: The container is tracking a hand.
 *
 * Enum that defines if the container is moving with user input.
 *
 **/
typedef enum
{
  G3K_CONTAINER_ATTACHMENT_NONE,
  G3K_CONTAINER_ATTACHMENT_HEAD,
  G3K_CONTAINER_ATTACHMENT_HAND
} G3kContainerAttachment;

/**
 * G3kContainerLayout:
 * @G3K_CONTAINER_NO_LAYOUT: No layout is set.
 * @G3K_CONTAINER_HORIZONTAL: A horizontal linear layout.
 * @G3K_CONTAINER_VERTICAL: A vertical linear layout.
 * @G3K_CONTAINER_RELATIVE: A relative layout.
 *
 * Defines how the children of a #G3kContainer are layed out.
 *
 **/
typedef enum
{
  G3K_CONTAINER_NO_LAYOUT,
  G3K_CONTAINER_HORIZONTAL,
  G3K_CONTAINER_VERTICAL,
  G3K_CONTAINER_RELATIVE
} G3kContainerLayout;

struct _G3kContainer;

G3kContainer *
g3k_container_new (G3kContext *context);

void
g3k_container_add_child (G3kContainer      *self,
                         G3kObject         *object,
                         graphene_matrix_t *relative_transform);

void
g3k_container_remove_child (G3kContainer *self, G3kObject *object);

void
g3k_container_set_distance (G3kContainer *self, float distance);

float
g3k_container_get_distance (G3kContainer *self);

void
g3k_container_set_attachment (G3kContainer          *self,
                              G3kContainerAttachment attachment,
                              GxrController         *controller);

void
g3k_container_set_layout (G3kContainer *self, G3kContainerLayout layout);

gboolean
g3k_container_step (G3kContainer *self);

void
g3k_container_hide (G3kContainer *self);

void
g3k_container_show (G3kContainer *self);

gboolean
g3k_container_is_visible (G3kContainer *self);

GSList *
g3k_container_get_children (G3kContainer *self);

void
g3k_container_center_view (G3kContainer *self, float distance);

G_END_DECLS

#endif /* g3k_container_H_ */
