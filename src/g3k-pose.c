/*
 * xrdesktop
 * Copyright 2022 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-pose.h"

#include <graphene-ext.h>

void
g3k_pose_init_from_pose (G3kPose *pose, G3kPose *other)
{
  graphene_point3d_init_from_point (&pose->position, &other->position);
  graphene_quaternion_init_from_quaternion (&pose->rotation, &other->rotation);
}

G3kPose
g3k_pose_new (graphene_point3d_t *position, graphene_quaternion_t *rotation)
{
  G3kPose ret;

  if (position)
    graphene_point3d_init_from_point (&ret.position, position);
  else
    ret.position = GRAPHENE_POINT3D_INIT_ZERO;

  if (rotation)
    graphene_quaternion_init_from_quaternion (&ret.rotation, rotation);
  else
    graphene_quaternion_init_identity (&ret.rotation);

  return ret;
}

void
g3k_pose_to_matrix (G3kPose *pose, graphene_matrix_t *out_transform)
{
  graphene_matrix_init_identity (out_transform);
  graphene_matrix_rotate_quaternion (out_transform, &pose->rotation);
  graphene_matrix_translate (out_transform, &pose->position);
}

G3kPose
g3k_pose_new_from_matrix (graphene_matrix_t *m)
{
  graphene_point3d_t    position;
  graphene_quaternion_t rotation;

  graphene_point3d_t unused_scale;
  graphene_ext_matrix_decompose (m, &unused_scale, &rotation, &position);

  return g3k_pose_new (&position, &rotation);
}

gboolean
g3k_pose_validate (G3kPose *p)
{
  if (!graphene_ext_point3d_validate (&p->position)
      || !graphene_ext_quaternion_validate (&p->rotation))
    {
      g_assert ("Pose validation failed" && FALSE);
      return FALSE;
    }
  return TRUE;
}
