/*
 * gulkan
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#version 460

#extension GL_EXT_multiview : enable

layout (binding = 0) uniform Transformation { mat4 mvp[2]; }
transformation;

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 uv;

layout (location = 0) out vec2 out_uv;

out gl_PerVertex { vec4 gl_Position; };

void
main ()
{
  gl_Position = transformation.mvp[gl_ViewIndex] * vec4 (position, 1.0f);
  gl_Position.y = -gl_Position.y;
  out_uv = uv;
}
