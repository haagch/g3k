/*
 * xrdesktop
 * Copyright 2021 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <g3k.h>

static void
_test_type_string ()
{
  G3kContext *context = g3k_context_new ();
  G3kTip     *pointer_tip = g3k_tip_new (context);

  g_print ("Object type %s\n",
           g_type_name (G_TYPE_FROM_INSTANCE (pointer_tip)));

  g_object_unref (pointer_tip);
  g_object_unref (context);
}

int
main ()
{
  _test_type_string ();

  return 0;
}
