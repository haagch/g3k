#include <g3k.h>

int
main ()
{
  G3kContext *g3k = g3k_context_new ();

  for (uint32_t i = 0; i < 10; i++)
    {
      g3k_context_play_sound (g3k, "window-attention");
      g_usleep (100000);
    }

  g_object_unref (g3k);

  return 0;
}
