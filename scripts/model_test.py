#!/usr/bin/env python3

# xrdesktop
# Copyright 2021 Collabora Ltd.
# Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
# SPDX-License-Identifier: MIT

import subprocess
import os
import time
from tabulate import tabulate
import glob
import sys

env = {}
for key in ['XDG_RUNTIME_DIR', 'VK_INSTANCE_LAYERS', 'XR_RUNTIME_JSON']:
    env[key] = os.environ[key]

if "XR_RUNTIME_JSON" not in env:
    print("You did not provide a XR_RUNTIME_JSON.")

def run_model(path, headless: bool = False):

    if headless:
        exec_path = "./build/examples/load_gltf_headless"
    else:
        exec_path = "./build/examples/load_gltf"

    process = subprocess.Popen([exec_path, path], env=env,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    warnings = []

    if not headless:
        try:
            process.wait(timeout=3.0)
        except subprocess.TimeoutExpired:
            process.terminate()

    stdout_bin, stderr_bin = process.communicate()

    try:
        stdout = stdout_bin.decode()
    except UnicodeDecodeError as e:
        print("UnicodeDecodeError:", e)
        stdout = ""

    try:
        stderr = stderr_bin.decode()
    except UnicodeDecodeError as e:
        print("UnicodeDecodeError:", e)
        stderr = ""

    if "VUID" in stdout:
        warnings.append("VK_ERROR")
    if "Unsupported attribute" in stderr:
        warnings.append("ATTRIBUTE_TYPE")
    if "Unsupported index" in stderr:
        warnings.append("INDEX_TYPE")

    # Unsupported attribs
    if "unsupported attrib 'TANGENT'" in stderr:
        warnings.append("TANGENTS")
    elif "unsupported attrib 'TEXCOORD" in stderr:
        warnings.append("MULTI_TEXCOORD")
    elif "unsupported attrib 'WEIGHTS" in stderr:
        warnings.append("WEIGHTS")
    elif "unsupported attrib 'JOINTS" in stderr:
        warnings.append("JOINTS")
    elif "unsupported attrib" in stderr:
        warnings.append("UNKNOWN_ATTRIB")

    if "heap-buffer-overflow" in stderr:
        warnings.append("ASAN heap-buffer-overflow")
    elif "SEGV" in stderr:
        warnings.append("ASAN SEGV")
    elif "LeakSanitizer" in stderr:
        warnings.append("ASAN_LEAKS")
    elif "AddressSanitizer" in stderr:
        warnings.append("UNKNOWN_ASAN")

    if "VK_EXT_index_type_uint8" in stderr:
        warnings.append("UINT8_INDEX")
    elif "required GLTF" in stderr:
        warnings.append("REQUIRED_EXT")
    elif "used GLTF" in stderr:
        warnings.append("USED_EXT")
    elif "size mismatch" in stderr:
        warnings.append("UNEXPECTED_SIZE")

    if "Unexpected stride" in stderr:
        warnings.append("UNEXPECTED_STRIDE")

    if "GLib-GObject-CRITICAL" in stderr:
        warnings.append("GLIB_CRITICAL")

    if "load_gltf_headless" in stderr:
        warnings.append("OUR_LEAK")

    return process.returncode, warnings


class colors:
    OK = '\033[92m'
    WARNING = '\033[93m'
    ERROR = '\033[91m'
    CRASH = '\033[35m'
    RESET = '\033[0m'



def ret_to_str(ret, warnings):
    warn_str = ""
    if warnings:
        warn_str = colors.WARNING
        for warn in warnings:
            warn_str += warn + " "
        warn_str += colors.RESET

    ret_str = ""
    if ret == -15 or ret == 0:
        ret_str = f"{colors.OK}LOADS{colors.RESET}"
    elif ret == -11 or ret == -6:
        ret_str = f"{colors.CRASH}CRASH{colors.RESET}"
    elif ret == 1:
        ret_str = f"{colors.ERROR}ERROR{colors.RESET}"
    else:
        ret_str = f"{colors.ERROR}UNKNOWN {ret}{colors.RESET}"

    return f"{ret_str} {warn_str}"


def find_models(model_dir, filter_files=False):
    files = glob.glob(f'{model_dir}/**/*.gltf', recursive=True)
    # files += glob.glob(f'{model_dir}/**/*.glb', recursive=True)

    if not filter_files:
        files.sort()
        return files


    ignore_list = ["Draco", "Embedded", "Quantized", "IBL"]
    filtered_files = []

    for file in files:
        ignore = False
        for word in ignore_list:
            if word in file:
                ignore = True
        if not ignore:
            filtered_files.append(file)

    filtered_files.sort()

    return filtered_files


def print_usage():
    print("Usage:")
    print(f"{sys.argv[0]} <model_dir>")


def main():
    if len(sys.argv) != 2:
        print_usage()
        return

    models_dir = sys.argv[1]
    models = find_models(models_dir, True)

    table = []

    for model in models:
        print(f"Loading '{model}'...")
        ret, warnings = run_model(model, headless=True)
        file_size = os.path.getsize(model)
        ret_str = ret_to_str(ret, warnings)
        print(ret_str)
        table.append((ret_str, file_size, model.replace(models_dir, "")))

    table.sort()
    print(tabulate(table))


main()
